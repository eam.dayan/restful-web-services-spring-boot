package com.ksga.restdemo.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Cafe API")
                                .description("This is my cafe.")
                                .version("1.0")
                                .contact(new Contact()
                                        .name("Dayan")
                                        .url("https://youtube.com/playlist?list=PLF2cRAAApyfA6tiQrl_5WuOsk-r6syNky")
                                        .email("eam.dayan@gmail.com")
                                )
//                                .termsOfService("TOC")
                                .license(new License().name("License").url("#"))
                );
    }
}
