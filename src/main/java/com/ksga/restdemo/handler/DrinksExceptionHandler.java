package com.ksga.restdemo.handler;

import com.ksga.restdemo.model.response.ErrorMessage;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DrinksExceptionHandler {

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<?> handleIllegalState(IllegalStateException e) {
        ErrorMessage error = ErrorMessage
                .builder()
                .status("error")
                .message(e.getMessage())
                .build();

        return ResponseEntity.badRequest()
                .body(error);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleIllegalState(NotFoundException e) {
           ErrorMessage error = ErrorMessage
                .builder()
                .status("error")
                .message(e.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }
}
