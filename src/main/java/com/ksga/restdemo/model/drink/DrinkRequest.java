package com.ksga.restdemo.model.drink;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DrinkRequest {
    private final String name;
    private final Double price;
}
