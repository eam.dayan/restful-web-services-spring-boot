package com.ksga.restdemo.model.drink;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Drink {
    private Integer id;
    private String name;
    private Double price;
}
