package com.ksga.restdemo.controller;

import com.ksga.restdemo.model.appuser.AppUserRequest;
import com.ksga.restdemo.service.AppUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private final AppUserService appUserService;

    public AuthController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping("/basic/login")
    public String getLoginPage(Model model){
        return "auth/login";
    }

    @GetMapping("/registration")
    public String getRegistrationPage(Model model){
        model.addAttribute("user", new AppUserRequest());
        return "auth/registration";
    }

    @PostMapping("/registration")
    public String registerNewUser(@ModelAttribute AppUserRequest appUserRequest){
        appUserService.insert(appUserRequest);
        return "redirect:/";
    }
}
