package com.ksga.restdemo.controller;

import com.ksga.restdemo.service.DrinkService;
import com.ksga.restdemo.model.drink.Drink;
import com.ksga.restdemo.model.drink.DrinkRequest;
import com.ksga.restdemo.model.drink.DrinkUpdateRequest;
import com.ksga.restdemo.model.response.Response;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/drinks")
@SecurityRequirement(name = "bearerAuth")
public class DrinkController {

    private final DrinkService drinkService;

    public DrinkController(DrinkService drinkService) {
        this.drinkService = drinkService;
    }

    @GetMapping("/hi")
    public ResponseEntity<?> hi() {
        return ResponseEntity.ok("hi");
    }

    @GetMapping
    public ResponseEntity<?> getAllDrinks(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "5") Integer size
    ) {
        if (page <= 0) throw new IllegalStateException("Page cannot be zero or negative");
        if (size <= 0) throw new IllegalStateException("Size cannot be zero or negative");

        Response<List<Drink>> response = Response
                .<List<Drink>>builder()
                .timeRequested(LocalDateTime.now())
                .data(drinkService.findAll(page, size))
                .status("success")
                .message(null)
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDrinkById(@PathVariable Integer id) throws NotFoundException {
        Response<Drink> response = Response
                .<Drink>builder()
                .timeRequested(LocalDateTime.now())
                .data(drinkService.findById(id))
                .status("success")
                .message(null)
                .build();
        if (response.getData() == null) {
            throw new NotFoundException("Unable to find drink with id " + id);
        }

        return ResponseEntity.ok().body(response);
    }

    @PostMapping
    public ResponseEntity<?> insertNewDrink(
            @RequestBody DrinkRequest drinkRequest,
            UriComponentsBuilder b
    ) {
        Integer id = drinkService.addDrink(drinkRequest);
//        UriComponents uriComponents =
//                b.path("/api/v1/drinks/{id}").buildAndExpand(id);
//        return ResponseEntity.created(uriComponents.toUri()).build();

        Response<Drink> response = Response
                .<Drink>builder()
                .timeRequested(LocalDateTime.now())
                .data(drinkService.findById(id))
                .status("success")
                .message(null)
                .build();
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDrink(@PathVariable Integer id) throws NotFoundException {
        drinkService.remove(id);
        Response<Drink> response = Response
                .<Drink>builder()
                .timeRequested(LocalDateTime.now())
                .data(null)
                .status("success")
                .message("Drink with id " + id + " has been successfully removed")
                .build();
        return ResponseEntity.ok().body(response);
    }

    @PutMapping
    public ResponseEntity<?> updateDrinkDetails(
            @RequestBody DrinkUpdateRequest drink
    ) throws NotFoundException {
        Integer id = drinkService.update(drink);

        Response<Drink> response = Response
                .<Drink>builder()
                .timeRequested(LocalDateTime.now())
                .data(null)
                .status("success")
                .message("Successfully updated drink with id " + id)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
